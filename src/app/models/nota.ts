export class Nota {
    _id : string;
    titulo: string ;
    descripcion:string;
    palabra_clave:  Array<any>;
    hashtag: Array <any>;
    cuerpo: string;
    imagen_portada: string;
    seccion: string;
    usuario: string;
}
