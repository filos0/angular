export class Rol {
    "name": string;
    "_id": string;
    "display_name": string;
    "description": string;
    "created": Date;
}
