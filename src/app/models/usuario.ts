export class Usuario {
    id: string;
    nombre: string;
    ap_paterno: string;
    ap_materno: string;
    email: string;
    usuario: string;
    token:string;
    avatar:string;
    secciones: Array<any>;
    roles: Array<any>;
    status:boolean;
}
