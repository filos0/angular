export class Seccion {
    
    "estatus":string;
    "_id": string;
    "nombre": string;
    "descripcion": string;
    "subseccion": Array<any>;
    "created": Date;
}
