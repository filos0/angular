import { Routes, RouterModule } from '@angular/router';

//Route for content layout with sidebar, navbar and footer
export const Full_ROUTES: Routes = [
  {
    path: 'home',
    loadChildren: () => import('../../componentes/home/home-module').then(m => m.HomeModule)
  },
  {
    path: '',
    loadChildren: () => import('../../componentes/notas/notas-module').then(m => m.NotasModule)
  },
  {
    path: '',
    loadChildren: () => import('../../componentes/usuarios/usuarios-module').then(m => m.UsuariosModule)
  },
  {
    path: '',
    loadChildren: () => import('../../componentes/catalogos/catalogos-module').then(m => m.CatalogosModule)
  }
  
];
