import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Global } from '../../services/global';
import { Usuario } from '../../models/usuario';

@Injectable({ providedIn: 'root' })

export class AuthService {

  public url: string;
  private currentUserSubject: BehaviorSubject<Usuario>;
  public currentUser: Observable<Usuario>;


  constructor(private _http: HttpClient, private router: Router) {
    this.url = Global.url;
    this.currentUserSubject = new BehaviorSubject<Usuario>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();

  }
  public get currentUserValue(): Usuario {

    return this.currentUserSubject.value;
  }

  signinUser(user: string, pass: string) {
    
    let params = JSON.stringify({"usuario": user,"password": pass});
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    
    return this._http.post<any>(this.url + 'login', params, { headers: headers }).pipe
    (map(Usuario => {
                      localStorage.setItem('currentUser', JSON.stringify(Usuario));
                      this.currentUserSubject.next(Usuario);
                      return Usuario;}));
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.clear();
    this.currentUserSubject.next(null);
  }

}
