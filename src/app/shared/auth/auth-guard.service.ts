import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Alert } from 'selenium-webdriver';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(
              private authService: AuthService,
              private router: Router
            ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authService.currentUserValue;
    if (currentUser) {
        return true;
    }

    this.router.navigate(['login']);
    return false;
  }
}
