import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [

    {
        path: '/home',
        title: 'Inicio',
        icon: 'ft-home',
        class: '',
        badge: '',
        badgeClass: '',
        isExternalLink: false,
        submenu: []
    },
    {
        path: '',
        title: 'Notas',
        icon: 'ft-layout',
        class: 'has-sub',
        badge: '',
        badgeClass: '',
        isExternalLink: false,
        submenu: [{
            path: '/nueva-nota',
            title: 'Nueva Nota',
            icon: 'icon-note',
            class: '',
            badge: '',
            badgeClass: '',
            isExternalLink: false,
            submenu: []
        },
        {
            path: '/notas',
            title: 'Mis Notas',
            icon: 'icon-list',
            class: '',
            badge: '',
            badgeClass: '',
            isExternalLink: false,
            submenu: []
        },]
    },
    
    {
        path: '',
        title: 'Usuarios',
        icon: 'ft-users',
        class: 'has-sub',
        badge: '',
        badgeClass: '',
        isExternalLink: false,
        submenu: [
            {
                path: '/nuevo-usuario',
                title: 'Nuevo Usuario',
                icon: 'icon-user-follow',
                class: '',
                badge: '',
                badgeClass: '',
                isExternalLink: false,
                submenu: []
            }
        ]
    },
    {
        path: '',
        title: 'Catalogos',
        icon: 'ft-package',
        class: 'has-sub',
        badge: '',
        badgeClass: '',
        isExternalLink: false,
        submenu: [
            {
                path: '',
                title: 'Secciones',
                icon: 'icon-music-tone-alt',
                class: 'has-sub',
                badge: '',
                badgeClass: '',
                isExternalLink: false,
                submenu: [    {
                    path: '/nueva-seccion',
                    title: 'Nueva sección',
                    icon: 'ft-plus',
                    class: '',
                    badge: '',
                    badgeClass: '',
                    isExternalLink: false,
                    submenu: []
                },
                {
                    path: '/secciones',
                    title: 'Lista secciones',
                    icon: 'icon-list',
                    class: '',
                    badge: '',
                    badgeClass: '',
                    isExternalLink: false,
                    submenu: []
                }
            ]
            }
        ]
    },
];
