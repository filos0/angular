import { RouteInfo } from './sidebar.metadata';

export const ROUTESC: RouteInfo[] = [

    {
        path: '/home',
        title: 'Inicio',
        icon: 'ft-home',
        class: '',
        badge: '',
        badgeClass: '',
        isExternalLink: false,
        submenu: []
    },
    {
        path: '',
        title: 'Notas',
        icon: 'ft-layout',
        class: 'has-sub',
        badge: '',
        badgeClass: '',
        isExternalLink: false,
        submenu: [{
            path: '/nueva-nota',
            title: 'Nueva Nota',
            icon: 'icon-note',
            class: '',
            badge: '',
            badgeClass: '',
            isExternalLink: false,
            submenu: []
        },
        {
            path: '/notas',
            title: 'Mis Notas',
            icon: 'icon-list',
            class: '',
            badge: '',
            badgeClass: '',
            isExternalLink: false,
            submenu: []
        },]
    },
    
];
