import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../shared/auth/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login-component.html',
  providers: [AuthService]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  logged = false;
  returnUrl: string;
  error = '';

  constructor(
    private zone:NgZone,
    private _formBuilder: FormBuilder,
    private _route: ActivatedRoute,
    private _router: Router,
    private _authService: AuthService
  ) {
    if (this._authService.currentUserValue !== null) {
      this._router.navigate(['home']);
    }
  }
  forzar(){
    window.location.replace("/");
  }

  ngOnInit() {
    
    this.loginForm = this._formBuilder.group({
      usuario: ['', Validators.required],
      contrasena: ['', Validators.required]
    });


  }
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    else {
      this._authService.signinUser(this.f.usuario.value, this.f.contrasena.value)
        .pipe(first()).subscribe(
          data => {
            this.error = '';
            this.logged = true;
            window.location.replace("/");
            //this._router.navigate(['home']);
            
          },
          error => {
            this.error = error.error.message
          
          }
        );
    }



  }

}
