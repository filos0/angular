import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthRoutingModule} from './auth-routing.module';
import { FormGroup , FormControl , ReactiveFormsModule , FormsModule } from '@angular/forms';

import {LoginComponent} from './login/login-component';

@NgModule({
    imports: [
        CommonModule,
        AuthRoutingModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        LoginComponent
    ]
})
export class AuthModule {
}
