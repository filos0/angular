import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill'
import { NgSelectModule } from '@ng-select/ng-select';
import { TagInputModule } from 'ngx-chips';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';
import { UsuariosComponent } from './usuarios/usuarios.component';


@NgModule({
    imports: [
        CommonModule,
        UsuariosRoutingModule,
        QuillModule.forRoot(),
        ReactiveFormsModule,
        FormsModule,
        TagInputModule,
        NgSelectModule

    ],
    declarations: [
        UsuarioComponent,
        UsuariosComponent
    ]
})
export class UsuariosModule {
}
