import { Component, ViewChild, OnInit } from '@angular/core';
import { QuillEditorComponent } from 'ngx-quill';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../../../services/usuario-service';
import { RolService } from '../../../services/rol-service';
import { SeccionService } from '../../../services/seccion-service'
import { Rol } from '../../../models/rol';
import { Seccion } from '../../../models/seccion';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-usuario',
  templateUrl: './nuevo-usuario.component.html',
  providers: [UsuarioService, RolService, SeccionService],
  styleUrls: ['./nuevo-usuario.component.scss']
})
export class UsuarioComponent implements OnInit {

  public _usuario: any;
  public roles: Rol[]
  public secciones: Seccion[];
  @ViewChild('editor', { static: true }) editor: QuillEditorComponent;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _usuarioService: UsuarioService,
    private _rolService: RolService,
    private _seccionService: SeccionService
  ) {
    this._usuario = {
      nombre: '',
      apellidoPaterno: '',
      apellidoMaterno: '',
      contrasena: this.randPassword(3, 2, 3),
      correo: '',
      user: '',
      rol: '',
      seccion: ''

    }
    this._rolService.getRol().subscribe(
      data => {
        this.roles = data.object
      },
      error => {
        console.log(error)
      }
    );
    this._seccionService.getSecciones().subscribe(
      data => {
        this.secciones = data.objects
      },
      error => {
        console.log(error)
      }
    )

  }

  onSubmit() {

    if (!this.validarSubmit()) {
      Swal.fire({
        title: 'Error',
        text: 'Hay campos que necesitas llenar',
        icon: 'error',
        showConfirmButton: false,
        showCancelButton: true,
        cancelButtonText: 'Aceptar',

      })
    }
    else {
      this._usuarioService.createUsuario(this._usuario).subscribe(
        response => {
          console.log(response)
          /*if (response.status) {
            Swal.fire({
              title: 'Exito',
              text: 'Tu usuario fue guardada correctamente',
              icon: 'success',
              confirmButtonText: 'Aceptar'
            }).then((result) => {
              if (result.value) {
                this._router.navigate(['/home']);
              }
            });
          }

          else {
            Swal.fire({
              title: 'Error',
              text: 'Error inesperado',
              icon: 'error',
              showConfirmButton: false,
              showCancelButton: true,
              cancelButtonText: 'Aceptar',

            });
          }*/
        },
        error => {
          Swal.fire({
            title: 'Error',
            text: error.message,
            icon: 'error',
            showConfirmButton: false,
            showCancelButton: true,
            cancelButtonText: 'Aceptar',
          });
        }
      )
    }
  }

  ngOnInit() {
  }

  validarSubmit() {

    if (this._usuario.nombre == "")
      return false;
    else if (this._usuario.apellidoMaterno == "")
      return false;
    else if (this._usuario.apellidoPaterno == "")
      return false;
    else if (this._usuario.contrasena == "")
      return false;
    else if (this._usuario.correo == "")
      return false;
    else if (this._usuario.rol == "")
      return false;
    else if (this._usuario.user = "")
      return false;
    else if (this._usuario.seccion == "")
      return false;
    else
      return true;
  }

  generarContrasena($event) {
    this._usuario.contrasena = this.randPassword(3, 3, 2)
  }

  randPassword(letters, numbers, either) {
    var chars = [
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
      "0123456789",
      "_-?!"
    ];

    return [letters, numbers, either].map(function (len, i) {
      return Array(len).fill(chars[i]).map(function (x) {
        return x[Math.floor(Math.random() * x.length)];
      }).join('');
    }).concat().join('').split('').sort(function () {
      return 0.5 - Math.random();
    }).join('')
  }

}
