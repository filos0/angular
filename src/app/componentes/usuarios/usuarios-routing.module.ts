import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { UsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';
import { UsuariosComponent } from './usuarios/usuarios.component';

const routes: Routes = [
    {
        path: 'nuevo-usuario',
        component: UsuarioComponent,
    },
    {
        path: 'usuarios',
        component: UsuariosComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UsuariosRoutingModule {
}
