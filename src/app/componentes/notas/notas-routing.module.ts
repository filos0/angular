import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { NotaComponent} from './nueva-nota/nueva-nota-component';
import { MisNotasComponent } from './mis-notas/mis-notas-component';
import { VerNotaComponent } from './ver-nota/ver-nota.component';

const routes: Routes = [
    {
        path: 'nueva-nota',
        component: NotaComponent,
    },
    {
        path: 'notas',
        component: MisNotasComponent,
    },
    {
        path: 'nota/:id',
        component: VerNotaComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class NotaRoutingModule {
}
