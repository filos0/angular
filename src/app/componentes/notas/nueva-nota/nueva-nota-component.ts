import { Component, ViewChild, OnInit } from '@angular/core';
import { QuillEditorComponent } from 'ngx-quill';
import { RouterModule, Router, ActivatedRoute } from '@angular/router'
import { NoticiaService } from './../../../services/noticia-service';
import { SeccionService } from '../../../services/seccion-service';
import { Seccion } from '../../../models/seccion';
import Swal from 'sweetalert2';


@Component({
    selector: 'app-nota',
    templateUrl: './nueva-nota-component.html',
    providers: [NoticiaService, SeccionService],
    styleUrls: ['./nueva-nota-component.scss']
})
export class NotaComponent implements OnInit {


    public nota: any;
    public optionsSecciones: Seccion[]; 
    @ViewChild('editor', { static: true }) editor: QuillEditorComponent;

    constructor(
        private _router: Router,
        private _route: ActivatedRoute,
        private _noticaService: NoticiaService,
        private _seccionService: SeccionService

    ) {
  

        this.nota = {
            titulo: '',
            descripcion: '',
            seccion: '',
            portada: '',
            hashtags: '',
            palabrasClaves: '',
            cuerpo: ''
        }

        this._seccionService.getSecciones().subscribe(
            data => {
              this.optionsSecciones = data.objects
            },
            error => {
              console.log(error)
            }
          )
    }

    onSubmit() {

        if (!this.validarSubmit())
            Swal.fire({
                title: 'Error',
                text: 'Hay campos que necesitas llenar',
                icon: 'error',
                showConfirmButton: false,
                showCancelButton: true,
                cancelButtonText: 'Aceptar',

            })
        else {
            this._noticaService.createNoticia(this.nota).subscribe(
                response => {
                    console.log(response)
                    
                    if (response.status)
                        Swal.fire({
                            title: 'Exito',
                            text: 'Tu nota fue guardada correctamente',
                            icon: 'success',
                            confirmButtonText: 'Aceptar'
                        }).then((result) => {
                            if (result.value) {
                                this._router.navigate(['/home']);
                            }
                        });
                    else {
                        Swal.fire({
                            title: 'Error',
                            text: 'Error inesperado',
                            icon: 'error',
                            showConfirmButton: false,
                            showCancelButton: true,
                            cancelButtonText: 'Aceptar',

                        });
                    }
                },
                error => {

                    Swal.fire({
                        title: 'Error',
                        text: error.message,
                        icon: 'error',
                        showConfirmButton: false,
                        showCancelButton: true,
                        cancelButtonText: 'Aceptar',

                    });
                }
            )
        }
    }

    ngOnInit() {
    }

    validarSubmit() {
        if (this.nota.titulo == "")
            return false;
        else if (this.nota.descripcion == "")
            return false;
        else if (this.nota.seccion == "")
            return false;
        else if (this.nota.portada == "")
            return false;
        else if (this.nota.palabrasClaves == "")
            return false;
        else if (this.nota.cuerpo == "")
            return false;
        else
            return true;
    }


}


