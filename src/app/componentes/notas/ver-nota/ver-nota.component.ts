import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { NoticiaService } from '../../../services/noticia-service';
import { Nota } from '../../../models/nota';

@Component({
  selector: 'app-ver-nota',
  providers: [NoticiaService],
  templateUrl: './ver-nota.component.html',
  styleUrls: ['./ver-nota.component.scss']
})
export class VerNotaComponent implements OnInit {
  public id: string
  public nota: Nota
  public cargado = true

  constructor(
    private route: ActivatedRoute,
    private _noticiasService: NoticiaService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id')
    this._noticiasService.getNoticia(this.id).subscribe(
      data => {
        this.cargado = true
        this.nota = data.objects
        console.log(this.nota)
      },
      error => { }
    )
  }

}
