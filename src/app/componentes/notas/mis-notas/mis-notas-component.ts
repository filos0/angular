import {Component, ViewChild, OnInit} from '@angular/core';
import {QuillEditorComponent} from 'ngx-quill';
import {NoticiaService} from '../../../services/noticia-service';
import { Nota } from '../../../models/nota';
@Component({
    selector: 'app-mis-notas',
    providers:[NoticiaService],
    templateUrl: './mis-notas-component.html',
    styleUrls: ['./mis-notas-component.scss']
})
export class MisNotasComponent implements OnInit {

    public nota: any;
    public Notas: Nota[];
    @ViewChild('editor', {static: true}) editor: QuillEditorComponent;

    constructor(private _noticiasService :NoticiaService ) {

    }

    onSubmit() {

        console.log(this.nota)
    }

    ngOnInit() {
        this._noticiasService.getNoticias().subscribe(
            data => {
                this.Notas = data.objects
                console.log(this.Notas)
            },
            error => {}
        )
    }


}


