import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { QuillModule } from 'ngx-quill'
import { NgSelectModule } from '@ng-select/ng-select';
import { TagInputModule } from 'ngx-chips';
import {NotaRoutingModule} from './notas-routing.module';
import {NotaComponent} from './nueva-nota/nueva-nota-component';
import { MisNotasComponent } from './mis-notas/mis-notas-component';
import { VerNotaComponent } from './ver-nota/ver-nota.component';



@NgModule({
    imports: [
        CommonModule,
        NotaRoutingModule,
        QuillModule.forRoot(),
        ReactiveFormsModule,
        FormsModule,
        TagInputModule,
        NgSelectModule,
    
    ],
    declarations: [
        NotaComponent,
        MisNotasComponent,
        VerNotaComponent
    ]
})
export class NotasModule {
}
