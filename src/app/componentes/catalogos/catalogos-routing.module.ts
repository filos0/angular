import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { SeccionComponent} from './secciones/nueva-seccion/nueva-seccion-component';
import { VerSeccionComponent } from './secciones/ver-secciones/ver-secciones-component';

const routes: Routes = [
    {
        path: 'nueva-seccion',
        component: SeccionComponent,
    },
    {
        path:'ver-secciones',
        component: VerSeccionComponent
    }
   
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CatalogosRoutingModule {
}
