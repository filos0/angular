import { Component, OnInit } from '@angular/core';
import { RouterModule, Router, ActivatedRoute } from '@angular/router'
import { SeccionService } from '../../../../services/seccion-service'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-seccion',
  templateUrl: './nueva-seccion-component.html',
  providers: [SeccionService],
  styleUrls: ['./nueva-seccion-component.scss']
})
export class SeccionComponent implements OnInit {

  public seccion: any;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _seccionService: SeccionService
  ) {

    this.seccion = {
      nombre: '',
      descripcion: '',
      subsecciones: ''
    }
  }

  onSubmit() {
    if (!this.validarSubmit()) {
      Swal.fire({
        title: 'Error',
        text: 'Hay campos que necesitas llenar',
        icon: 'error',
        showConfirmButton: false,
        showCancelButton: true,
        cancelButtonText: 'Aceptar',

      })
    }
    else {
      this._seccionService.createSeccion(this.seccion.nombre, this.seccion.descripcion, this.seccion.subsecciones).subscribe(
        data => {
    
          if (data.status) {
            Swal.fire({
              title: 'Exito',
              text: 'Tu seccion fue guardada correctamente',
              icon: 'success',
              confirmButtonText: 'Aceptar'
            }).then((result) => {
              if (result.value) {
                this._router.navigate(['/home']);
              }
            });
          }
          else {
            Swal.fire({
              title: 'Error',
              text: 'Error inesperado',
              icon: 'error',
              showConfirmButton: false,
              showCancelButton: true,
              cancelButtonText: 'Aceptar',

            });
          } 
        },
        error => {
          Swal.fire({
            title: 'Error',
            text: error.message,
            icon: 'error',
            showConfirmButton: false,
            showCancelButton: true,
            cancelButtonText: 'Aceptar',
          });
        }
      )

    }
  }

  ngOnInit() {
  }

  validarSubmit() {
    if (this.seccion.nombre == "")
      return false;
    else if (this.seccion.descripcion == "")
      return false;
    else if (this.seccion.subsecciones == "")
      return false;
    else
      return true;
  }

}
