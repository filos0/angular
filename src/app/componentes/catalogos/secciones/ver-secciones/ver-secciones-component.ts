import { Component, OnInit } from '@angular/core';
import { RouterModule, Router, ActivatedRoute } from '@angular/router'
import { SeccionService } from '../../../../services/seccion-service'
import { Seccion } from '../../../../models/seccion'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-seccion',
  templateUrl: './ver-secciones-component.html',
  providers: [SeccionService]
})
export class VerSeccionComponent implements OnInit {

  public seccion: any;
  public Secciones: Seccion[];
  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _seccionService: SeccionService
  ) {
  }


  ngOnInit() {
    this._seccionService.getSecciones().subscribe(
      data => {
       
        this.Secciones = data.objects
        console.log(this.Secciones);
      },
      error => {
        console.log(error)
      }
    )
  }

}
