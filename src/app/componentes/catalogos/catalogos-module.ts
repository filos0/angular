import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { QuillModule } from 'ngx-quill'
import { NgSelectModule } from '@ng-select/ng-select';
import { TagInputModule } from 'ngx-chips';
import {CatalogosRoutingModule} from './catalogos-routing.module';
import { SeccionComponent } from './secciones/nueva-seccion/nueva-seccion-component';
import { VerSeccionComponent } from './secciones/ver-secciones/ver-secciones-component';

@NgModule({
    imports: [
        CommonModule,
        CatalogosRoutingModule,
        QuillModule.forRoot(),
        ReactiveFormsModule,
        FormsModule,
        TagInputModule,
        NgSelectModule,
    
    ],
    declarations: [
        SeccionComponent,
        VerSeccionComponent
    ]
})
export class CatalogosModule {
}
