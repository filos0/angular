import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from "rxjs";
import { Global } from './global'
import { AuthService } from '../../app/shared/auth/auth.service'
@Injectable()

export class NoticiaService{
    
    public url : string;
    public _hashtags: any = []
    public _palabrasClaves: any = []
    constructor(private _http : HttpClient, private _authService : AuthService){
        this.url = Global.url;
    }
    createNoticia(Nota):Observable<any>{
        
        Nota.hashtags.forEach((sec, index) => {
            this._hashtags.push(sec.value)
        });
        Nota.palabrasClaves.forEach((sec, index) => {
            this._palabrasClaves.push(sec.value)
        });
        console.log(Nota)
        let params = JSON.stringify({
            titulo: Nota.titulo ,
            descripcion:Nota.descripcion,
            hashtag: this._hashtags,
            palabra_clave: this._palabrasClaves,
            cuerpo: Nota.cuerpo,
            imagen_portada: "",
            seccion: Nota.seccion,
            usuario: this._authService.currentUserValue.id,
        });
    
        let headers = new HttpHeaders().set('Content-Type','application/json');
        return this._http.post(this.url+'noticia',params,{headers : headers});
    }  
    getNoticias():Observable<any>{
        return this._http.get(this.url+'noticias');
    }
    getNoticia(noticia_id):Observable<any>{
        return this._http.get(this.url+'noticia/'+noticia_id);
    }
}