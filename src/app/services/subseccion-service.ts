import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from "rxjs";
import { Global } from './global'
@Injectable()

export class SubSeccionService{
    
    public url : string;
    constructor(private _http : HttpClient){
        this.url = Global.url;
    }    
    getSubSecciones():Observable<any>{
        return this._http.get(this.url+'secciones');
    }
    getSubseccion(seccion_id):Observable<any>{
        return this._http.get(this.url+'seccion/'+seccion_id);
    }

}