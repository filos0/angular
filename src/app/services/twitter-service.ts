import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from "rxjs";

@Injectable()

export class TwitterService {

    constructor(private _http: HttpClient) {}        

    getTrendsTopic(): Observable<any> {
        let headers = new HttpHeaders().set('upgrade-insecure-requests','1');
        return this._http.get("https://twitter.com/hashtag/toptrending",{headers : headers});
    }

}