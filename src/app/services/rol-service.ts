import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from "rxjs";
import {AuthService} from '../shared/auth/auth.service';
import { Global } from './global'
@Injectable()

export class RolService {

    public url: string;
    constructor(private _http: HttpClient, private _authService: AuthService) {
        this.url = Global.url;
    }
    getRol(): Observable<any> {
        return this._http.get(this.url + 'rol');
    }
}