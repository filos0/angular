import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from "rxjs";
import {AuthService} from '../shared/auth/auth.service';
import { Global } from './global'
@Injectable()

export class UsuarioService {

    public url: string;
    constructor(private _http: HttpClient, private _authService: AuthService) {
        this.url = Global.url;
    }
    getCurrentUsuario(){
        return this._authService;
    }
    getUsuarios(): Observable<any> {
        return this._http.get(this.url + 'usuarios');
    }
    getUsuario(Usuario_id): Observable<any> {
        return this._http.get(this.url + 'usuario/' + Usuario_id);
    }
    createUsuario(_Usuario): Observable<any> {
    
        let params = JSON.stringify({
            nombre: _Usuario.nombre,
            ap_paterno: _Usuario.apellidoPaterno,
            ap_materno: _Usuario.apellidoMaterno,
            email: _Usuario.correo,
            usuario: _Usuario.user,
            contrasena: _Usuario.contrasena,
            roles: _Usuario.rol,
            seccion: _Usuario.seccion
        });
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.post(this.url + 'usuario', params, { headers: headers });
    }
}