import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from "rxjs";
import { Global } from './global'
@Injectable()

export class SeccionService {

    public url: string;
    public subsecciones: any = []
    constructor(private _http: HttpClient) {
        this.url = Global.url;
    }

    getSecciones(): Observable<any> {
        return this._http.get(this.url + 'secciones');
    }
    getSeccion(seccion_id: string): Observable<any> {
        return this._http.get(this.url + 'seccion/' + seccion_id);
    }
    createSeccion(nombre: string, descripcion: string, subseccion: Array<any>): Observable<any> {
        //this.subsecciones=subseccion
        subseccion.forEach((sec, index) => {
            this.subsecciones.push(sec.value)
        });
        let params = JSON.stringify({
            nombre: nombre,
            descripcion: descripcion,
            subseccion: this.subsecciones
        });
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
    
        return this._http.post(this.url+'seccion',params,{headers : headers});
    }
}